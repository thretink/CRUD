<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TestingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getUser = User::get();
        return view('ViewUser',compact('getUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('FormUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        $post = User::create(
            [
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>md5($request->password),
            ]
        );
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\testing  $testing
     * @return \Illuminate\Http\Response
     */
    public function show(testing $testing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\testing  $testing
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getEdit = User::where('id',$id)->first();
        return view('FormEdit',compact('getEdit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\testing  $testing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $rr = $request->except('_method', '_token');
        $dataLama = User::where('id',$id)->first();
        $dataLama->update($rr);
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\testing  $testing
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id',$id)->delete();
        return back();
    }
}
