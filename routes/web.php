<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'App\Http\Controllers\TestingController@index');
Route::post('user/post', 'App\Http\Controllers\TestingController@store')->name('user.post');
Route::get('user/get', 'App\Http\Controllers\TestingController@index')->name('user.data');
Route::get('user/form', 'App\Http\Controllers\TestingController@create')->name('user.create');
Route::get('user/delete/{id}', 'App\Http\Controllers\TestingController@destroy')->name('user.delete');
Route::get('user/edit/{id}', 'App\Http\Controllers\TestingController@edit')->name('user.edit');
Route::put('user/{id}/edits', 'App\Http\Controllers\TestingController@update')->name('user.update');
