@extends('layout.base')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap5.min.css" crossorigin="anonymous">
@endsection

@section('content')
    <a href="{{route('user.create')}}">Tambah</a>
    <table id="example" class="table table-striped" style="width:100%">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Password</th>
            <th>Created_at</th>
            <th>Update_at</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($getUser as $data)

        <tr>
            <td>{{$data->name}}</td>
            <td>{{$data->email}}</td>
            <td>{{$data->password}}</td>
            <td>{{$data->created_at}}</td>
            <td>{{$data->updated_at}}</td>
            <td>
                <a href="{{route('user.edit',$data->id)}}">Edit</a>
                <a href="{{route('user.delete',$data->id)}}">Detele</a>
            </td>

        </tr>
        @endforeach
    </table>
@stop
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

@endsection
