@extends('layout.base')

@section('styles')

@endsection

@section('content')
    <form  class="form-material" method="POST" action="{{ route('user.post') }}"
           enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Name">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">email</label>
            <input type="email" name="email" class="form-control" id="Email" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">passord</label>
            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@stop
@section('scripts')

@endsection
